## Cinemapi - Cinema API Node Server

## Usage

Start app, hydra console and web proxy for local development:

```
make start
```

Watch for file changes and restart containers when changes occur:
```
make watch
```

To be able to access the app, you need to setup hostnames (see next section)

## Setup hostnames

Add the following line to your machines `/etc/hosts`:

```
127.0.0.1   screening-events.test hydra-console.test
```

After running `make start` you can access the app under `http://screening-events.test`
and the hydra-console under `http://hydra-console.test`

## Deployment to heroku

### Prerequisites
You need a heroku account and access to the [screening-events](https://dashboard.heroku.com/apps/screening-events) project.
You need to be logged in to heroku and the container registry:
```bash
heroku login
heroku container:login
```

### Release new version

To build and push an image to the registry run

```
make image
```

To release it run

```
make release
```

Or, to do both in one step run

```
make deploy
```
