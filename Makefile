image: ## build image and push to heroku registry
	heroku container:push web --app screening-events

release: ## release container to heroku
	heroku container:release web --app screening-events

deploy: image release

logs: ## show heroku logs
	heroku logs --tail --app screening-events

start: ## start application in a local docker container
	docker-compose up --force-recreate -d

watch: ## watch for file changes and restart local container
	inotifywait --monitor --recursive --event modify --format "make start" ./src | sh

.PHONY: jar image release deploy logs start watch
