require('@babel/polyfill');
const moment = require('moment');
const randomUuid = require('uuid/v4');
const express = require('express');
const app = express();

const entrypoint = require('./resources/entrypoint');
const context = require('./resources/context');
const vocab = require('./resources/vocab');
const guide = require('./resources/guide');
const events = require('./resources/events');
const cinemas = require('./resources/cinemas');
const orders = require('./resources/orders');

const {init: initializeData, mergeFacts} = require('./persistence/store');

const BASE_URI = process.env.BASE_URI;

const contextLastModified = (new Date()).toUTCString();

app.enable('etag');
app.use(express.json({
  type: ['application/json', 'application/ld+json'],
}));

app.get('/*', (req, res, next) => {
  res.header('Content-Type', 'application/ld+json' );
  res.header('Link',
      `<${BASE_URI}/vocab#>; rel="http://www.w3.org/ns/hydra/core#apiDocumentation"`);
  next();
});

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Expose-Headers', 'Location, Link');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/', function(req, res) {
  sendJsonLd(entrypoint(), res);
});

app.get('/vocab', function(req, res) {
  res.send(vocab);
});

app.get('/guide', function(req, res) {
  sendJsonLd(guide(), res);
});

app.get('/context.jsonld', (req, res) => {
  res.append('Last-Modified', contextLastModified);
  res.send(context);
});

app.get('/events/:id', (req, res) => {
  sendJsonLd(events(req.params.id), res);
});

app.get('/cinemas/:id', (req, res) => {
  sendJsonLd(cinemas(req.params.id), res);
});

app.post('/orders', (req, res) => {
  const uuid = randomUuid();
  const id = `${BASE_URI}/orders/${uuid}`;
  res.header('Location', id);
  const order = {
    '@context': context,
    '@id': id,
    '@type': 'Order',
    // TODO of course, some validation should be done here!
    'acceptedOffer': req.body,
    'orderDate': moment().seconds(0).milliseconds(0).toISOString(),
  };
  mergeFacts(order).then(() => mergeFacts({
    '@context': context,
    '@id': 'orders',
    'member': {
      '@id': id,
    },
  }).then(() => sendJsonLd(orders(uuid), res))
  );
});

app.get('/orders', (req, res) => {
  sendJsonLd(orders(), res);
});

app.get('/orders/:id', (req, res) => {
  sendJsonLd(orders(req.params.id), res);
});

const sendJsonLd = (promise, res) => promise.then((data) => {
  data['@context'] = `${BASE_URI}/context.jsonld`;
  res.send(data);
});

initializeData().then(() => {
  const port = process.env.PORT;
  console.log(`Server running on port ${port}`);
  app.listen(port);
});


