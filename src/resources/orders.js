const context = require('./context');

const {frameFacts} = require('../persistence/store');

const orders = (id) => id ? byId(id) : all();

const byId = (id) => frameFacts({
  '@context': context,
  '@id': `orders/${id}`,
  'acceptedOffer': {
    'offeredItem': {
      '@explicit': true,
      'startDate': {},
      'workPresented': {
        '@explicit': true,
        'name': {},
      },
    },
  },
  'memberOf': {
    '@explicit': true,
  },
});

const all = () => frameFacts({
  '@context': context,
  '@id': 'orders',
  'member': {
    'acceptedOffer': {
      '@explicit': true,
    },
  },
});
module.exports = orders;
