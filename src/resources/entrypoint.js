const {frameFacts} = require('../persistence/store');

const context = require('./context');

const entrypoint = () => frameFacts({
  '@context': context,
  '@id': '/',
  'collection': {
    '@explicit': true,
    '@requireAll': false,
    'title': {},
    'manages': {},
    'operation': {},
  },
});

module.exports = entrypoint;
