const context = require('./context');

const {frameFacts} = require('../persistence/store');

const guide = () => frameFacts({
  '@context': context,
  '@id': 'guide',
  'member': {
    '@explicit': true,
    'startDate': {},
    'eventOf': {
      '@explicit': true,
      'name': {},
    },
    'location': {
      '@explicit': true,
      'name': {},
    },
    'workPresented': {},
  },
});

module.exports = guide;
