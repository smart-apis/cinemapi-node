const vocab = {
  '@context': {
    '@vocab': 'http://www.w3.org/ns/hydra/core#',
    'schema': 'http://schema.org/',
  },
  '@id': '/vocab#',
  '@type': 'ApiDocumentation',
  'supportedClass': [
    {
      '@id': 'schema:MovieTheater',
      '@type': 'Class',
      'title': 'Kino',
    },
    {
      '@id': 'schema:ScreeningEvent',
      '@type': 'Class',
      'title': 'Filmvorführung',
    },
  ],
};

module.exports = vocab;
