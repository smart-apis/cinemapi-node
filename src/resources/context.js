const BASE_URI = process.env.BASE_URI;
const context = {
  '@context': {
    '@version': 1.1,
    '@vocab': 'http://schema.org/',
    '@base': BASE_URI,

    'eventOf': {
      '@type': '@id',
      '@reverse': 'event',
    },
    'location': {
      '@type': '@id',
    },

    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',

    'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
    'label': 'rdfs:label',
    'comment': 'rdfs:comment',

    'hydra': 'http://www.w3.org/ns/hydra/core#',
    'title': 'hydra:title',
    'manages': 'hydra:manages',
    'Collection': 'hydra:Collection',
    'collection': 'hydra:collection',
    'Operation': 'hydra:Operation',
    'operation': 'hydra:operation',
    'method': 'hydra:method',
    'member': 'hydra:member',
    'memberOf': {
      '@reverse': 'member',
    },
    'expects': {
      '@id': 'hydra:expects',
      '@type': '@vocab',
    },
    'returns': {
      '@id': 'hydra:returns',
      '@type': '@vocab',
    },
    'subject': {
      '@id': 'hydra:subject',
      '@type': '@id',
    },
    'property': {
      '@id': 'hydra:property',
      '@type': '@vocab',
    },
    'object': {
      '@id': 'hydra:object',
      '@type': '@vocab',
    },
  },
};

module.exports = context;
