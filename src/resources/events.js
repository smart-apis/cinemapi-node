const context = require('./context');

const {frameFacts} = require('../persistence/store');

const events = (id) => frameFacts({
  '@context': context,
  '@id': `events/${id}`,
  'location': {
    '@explicit': true,
    'name': {},
  },
  'workPresented': {
    '@explicit': true,
    'name': {},
  },
});

module.exports = events;
