const context = require('./context');

const {frameFacts} = require('../persistence/store');

const cinemas = (id) => frameFacts({
  '@context': context,
  '@id': `cinemas/${id}`,
  'event': {
    '@explicit': true,
    'startDate': {},
    'location': {
      '@explicit': true,
      'name': {},
    },
    'workPresented': {
      '@explicit': true,
      'name': {},
    },
  },
});

module.exports = cinemas;
