const createStore = require('hyperfact').default;
const jsonld = require('jsonld');
const context = require('../resources/context');
const initialData = require('./initial-data');

const facts = createStore(context);

const frameFacts = (frame) => jsonld.frame(facts.all(), frame);

const mergeFacts = (newFacts) => facts.merge(newFacts);

const init = () => facts.merge(initialData);

module.exports = {
  init,
  frameFacts,
  mergeFacts,
};
