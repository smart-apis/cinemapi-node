const context = require('../resources/context');


let offerNumber = 1;
const offer = (eventId, priceInEur) => ({
  '@id': `${eventId}/offers/${offerNumber++}`,
  '@type': 'Offer',
  'offeredItem': {
    '@id': eventId,
  },
  'priceSpecification': {
    '@id': `prices/eur/${priceInEur}`,
    '@type': 'PriceSpecification',
    'price': priceInEur,
    'priceCurrency': 'EUR',
  },
});

module.exports = {
  '@context': context,
  '@graph': [
    {
      '@id': '/',
      '@type': 'Entrypoint',
      'title': 'Cinemapi',
      'collection': [{
        '@id': 'guide',
      }, {
        '@id': 'orders',
      }],
    },
    {
      '@id': `cinemas/cc`,
      '@type': 'MovieTheater',
      'name': `Codecentric Office-Kino`,
    },
    {
      '@id': '/guide',
      '@type': 'Collection',
      'title': 'What\'s on?',
      'manages': [
        {
          '@id': '/guide#manages',
          'property': 'rdf:type',
          'object': 'ScreeningEvent',
        },
      ],
      'member': [
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/1-0',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-21T20:00',
          'workPresented': {
            '@id': 'https://cinema-json-ld.de-c1.cloudhub.io/api/movies/tt6259332',
            '@type': 'Movie',
          },
          'offers': offer('events/1-0', 12),
        },
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/1-1',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-21T22:00',
          'workPresented': {
            '@id': '/movies/aquaman',
            '@type': 'Movie',
            'name': 'Aquaman',
          },
          'offers': offer('events/1-1', 14),
        },
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/1-2',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-22T18:00',
          'workPresented': {
            '@id': '/movies/aquaman',
            '@type': 'Movie',
            'name': 'Aquaman',
          },
          'offers': offer('events/1-2', 16),
        },
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/2-0',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-20T19:00',
          'workPresented': {
            '@id': '/movies/robin-hood',
            '@type': 'Movie',
            'name': 'Robin Hood',
          },
          'offers': offer('events/2-0', 16),
        },
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/2-1',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-20T21:00',
          'workPresented': {
            '@id': '/movies/robin-hood',
            '@type': 'Movie',
            'name': 'Robin Hood',
          },
          'offers': offer('events/2-1', 15),
        },
        {
          '@type': 'ScreeningEvent',
          '@id': '/events/3',
          'eventOf': '/cinemas/cc',
          'location': '/cinemas/cc',
          'startDate': '2019-01-21T15:00',
          'workPresented': {
            '@id': '/movies/der-junge-muss-an-die-frische-luft',
            '@type': 'Movie',
            'name': 'Der Junge muss an die frische Luft',
          },
          'offers': offer('events/2-1', 10),
        },
      ],
    },
    {
      '@id': 'orders',
      '@type': 'Collection',
      'title': 'Your orders',
      'manages': [
        {
          '@id': '/orders#manages',
          'property': 'rdf:type',
          'object': 'Order',
        },
      ],
      'operation': {
        '@type': ['Operation', 'CreateAction'],
        'method': 'POST',
        'label': 'Order ticket',
        'comment': 'Creates a new order with an accepted offer for an event',
        'expects': 'Offer',
        'returns': 'Order',
      },
    },
  ],
};
